<?php

function makeInput($type, $id, $name, $label, $value = ''){
    return "<label for='$id'>$label</label><input value='$value' type='$type' name='$name' id='$id' />";
}

function makeHidden($type, $id, $name, $value = ''){
    return "<input value='$value' type='$type' name='$name' id='$id' />";
}

function makeTextArea($id, $name, $label, $value = ''){
    return "<label for='$id'>$label</label><textarea type='textarea' name='$name' id='$id'>$value</textarea>";
}

function getParam($key){
    return isset($_GET[$key]) ? $_GET[$key] : null;
}

<?php

    // start or resume session
    session_start();

    $loginForm = array(
        "username" => array("type" => "text", "name" => "username", "label" => "Username"),
        "password" => array("type" => "password", "name" => "password", "label" => "Password"),
        "submited" => array("type" => "hidden", "name" => "submited")
    );

    include "html_elements.php";

    $user = 'ciro';
    $pass = '123456';

    if(isset($_POST['submited'])){
        if($user == $_POST['username'] && $pass == $_POST['password']){
            // register user in session
            $_SESSION['user_logged_in'] = $user;

            header('Location: index.php');
            exit();

        }else{
            $error_msg = "Bad Credentials";
        }
    }

?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>PHP Fundamentals</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
  </head>
  <body>
    <div class="container">
        <h1>Hello, PHP Fundamentals</h1>
        <form action="" name="login_form" method="post">
            <?php
                if(isset($error_msg)){
                    echo "<div class='alert alert-error'>$error_msg</div>";
                }
            ?>
            <fieldset>
                <legend>Login Form</legend>
                <?php
                    foreach ($loginForm as $key => $value) {
                        switch ($value["type"]) {
                            case 'text':
                            case 'password':
                                echo makeInput($value["type"], $key, $value["name"], $value["label"], getParam($key)) . "<br/>";
                                break;
                            case 'textarea':
                                echo makeTextArea($key, $value["name"], $value["label"], getParam($key)) . "<br/>";
                                break;
                            case 'hidden':
                                echo makeHidden($value["type"], $key, $value["name"], 1) . "<br/>";
                                break;
                        }
                    }
                ?>
                <button type="submit" name="submit_btn" class="btn">Login</button>
            </fieldset>
        </form>
    </div>
    <script src="http://code.jquery.com/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>
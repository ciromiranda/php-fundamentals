<?php
    ini_set("display_errors", "on");
    // error_reporting(E_ALL);

    // destroys all of the data associated with the current session
    // if(isset($_COOKIE[session_name()])) {
    //     setcookie( session_name(), "", time()-3600, "/" );
    // }
    // $_SESSION = array();
    // session_destroy();

    session_start();

    // verify if user is logged in
    if(!isset($_SESSION['user_logged_in'])){
        header('Location: login.php');
        exit();
    }

    $contactForm = array(
        "name" => array("type" => "text", "name" => "name", "label" => "Nome"),
        "email" => array("type" => "text", "name" => "email", "label" => "Email"),
        "comment" => array("type" => "textarea", "name" => "comment", "label" => "Comment"),
        "city" => array("type" => "select", "name" => "city", "label" => "City",
            "options" => array (
                array("label" => "Porto", "value" => "porto_2"),
                array("label" => "Lisboa", "value" => "lisboa_1")
            )
        ),
        "submited" => array("type" => "hidden", "name" => "submited")
    );

    include "html_elements.php";

?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>PHP Fundamentals</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
  </head>
  <body>
    <div class="container">
        <h1>Hello, PHP Fundamentals</h1>
        <div class="pull-left" style="width: 50%">
            <form action="" name="contact_form" method="get">
                <fieldset>
                    <legend>Contact Form</legend>

                    <?php
                        foreach ($contactForm as $key => $value) {
                            switch ($value["type"]) {
                                case 'text':
                                    echo makeInput($value["type"], $key, $value["name"], $value["label"], getParam($key)) . "<br/>";
                                    break;
                                case 'textarea':
                                    echo makeTextArea($key, $value["name"], $value["label"], getParam($key)) . "<br/>";
                                    break;
                                case 'hidden':
                                    echo makeHidden($value["type"], $key, $value["name"], 1) . "<br/>";
                                    break;
                            }
                        }
                    ?>
                    <button type="submit" name="submit_btn" class="btn">Submit</button>
                </fieldset>
            </form>
        </div>
        <div class="pull-left" style="width: 50%">
            <table class="table table-bordered" style="margin-top: 3px">
                <thead>
                    <tr>
                      <th>Request</th>
                      <th>Name</th>
                      <th>Email</th>
                      <th>Comment</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        if(getParam('submited')){

                            //save submited values in a session array
                            $_SESSION["contacts"][] = array(
                                'name' => getParam('name'),
                                'email' => getParam('comment'),
                                'comment' => getParam('comment')
                            );
                        }

                        if(isset($_SESSION['contacts'])){

                            foreach ($_SESSION["contacts"] as $key => $value) {
                                echo '<tr>';
                                echo '<td>GET</td>';
                                echo '<td>'.$value['name'].'</td>';
                                echo '<td>'.$value['email'].'</td>';
                                echo '<td>'.$value['comment'].'</td>';
                                echo '</tr>';
                            }
                        }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
    <script src="http://code.jquery.com/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>